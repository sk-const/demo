// src/App.js
import React from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import Sidebar from './components/Sidebar';
import Home from './components/Home';
import Projects from './components/projects/Projects'; 
import ProjectDetails from './components/projects/ProjectDetails';
import ChantierDetails from './components/projects/ChantierDetails';
import Lots from './components/lots/Lots'; // Assuming you have a Lots component
import Settings from './components/Settings';
import SubcontractorCompany from './components/subcontractor/SubcontractorCompany';
import Users from './components/users/Users';
import UserDetails from './components/users/UserDetails';
import SubcontractorDetails from './components/projects/SubcontractorDetails';

const App = () => {
  return (
    <Router>
      <div className="app flex">
        <Sidebar />
        <div className="main-content flex-1">
          <Routes>
            <Route path="/home" element={<Home />} />
            <Route path="/projects" element={<Projects />} />
            <Route path="/projects/:projectId" element={<ProjectDetails />} />
            <Route path="/projects/:projectId/chantiers/:chantierId" element={<ChantierDetails />} />
            <Route path="/subcontractors/:lotId" element={<SubcontractorDetails />} />

            <Route path="/lots" element={<Lots />} />
            <Route path="/subcontractorCompany" element={<SubcontractorCompany />} />

            SubcontractorView
            <Route path="/users" element={<Users />} />
            <Route path="/users/:userId" element={<UserDetails />} />

            
            
            <Route path="/settings" element={<Settings />} />
            <Route path="*" element={<Navigate to="/home" />} />
          </Routes>
        </div>
      </div>
    </Router>
  );
};

export default App;
