import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import subcontractorData from '../../data/subcontractorCompanies.json';
import lotsData from '../../data/lots_constractor.json';

const SubcontractorDetails = () => {
  const emailHistoryData = [
    { id: 1, date: '2024-07-01T09:30:00', event: 'Email sent', details: 'Initial contact' },
    { id: 2, date: '2024-07-05T11:15:00', event: 'Email received', details: 'Follow-up response' },
    { id: 3, date: '2024-07-10T14:00:00', event: 'Email sent', details: 'Quote sent' },
    { id: 4, date: '2024-07-15T16:45:00', event: 'Email received', details: 'Negotiation' },
  ];

  const { lotId } = useParams(); // Assuming you get lotId from URL params
  const [subcontractors, setSubcontractors] = useState([]);
  const [selectedSubcontractor, setSelectedSubcontractor] = useState(null);
  const [showEmailHistory, setShowEmailHistory] = useState(false);
  const [showUploadPopup, setShowUploadPopup] = useState(false); // State for showing upload popup

  useEffect(() => {
    // Fetch subcontractors based on lotId from lotsData
    const lot = lotsData.find(lot => lot.id === parseInt(lotId));
    if (lot) {
      const subcontractorIds = lot.subcontractors;
      const filteredSubcontractors = subcontractorData.filter(sub => subcontractorIds.includes(sub.id));
      setSubcontractors(filteredSubcontractors);
    }
  }, [lotId]);

  const openEmailHistory = (subcontractor) => {
    setSelectedSubcontractor(subcontractor);
    setShowEmailHistory(true);
  };

  const closeEmailHistory = () => {
    setShowEmailHistory(false);
  };

  const handleResendEmail = () => {
    // Logic to handle email resend
    // For demonstration, simply show the upload popup
    setShowUploadPopup(true);
  };

  if (!lotId) {
    return <div>Loading...</div>; // Handle case where lotId is undefined
  }

  if (subcontractors.length === 0) {
    return <div>No subcontractors found for Lot {lotId}</div>;
  }

  return (
    <div className="p-6">
      <h2 className="text-2xl font-bold mb-4">Subcontractors for Lot {lotId}</h2>

      {/* Description and Preview File Section */}
      <div className="mb-6">
        <p className="text-lg mb-2">Description:</p>
        <p className="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis libero in augue tempus maximus. Fusce nec enim id quam dapibus suscipit.</p>
        <button
          className="bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded-md shadow-md"
          onClick={() => alert('Preview files functionality')}
        >
          Preview Files
        </button>
      </div>

      {/* Subcontractors Table */}
      <div className="overflow-x-auto">
        <table className="min-w-full bg-white border border-gray-200 rounded-lg">
          <thead className="bg-gray-100">
            <tr>
              <th className="px-4 py-2">Enterprise Name</th>
              <th className="px-4 py-2">Address</th>
              <th className="px-4 py-2">Lot</th>
              <th className="px-4 py-2">Contact</th>
              <th className="px-4 py-2">Email</th>
              <th className="px-4 py-2">Actions</th>
            </tr>
          </thead>
          <tbody>
            {subcontractors.map(subcontractor => (
              <tr key={subcontractor.id} className="hover:bg-gray-50">
                <td className="border px-4 py-2">{subcontractor.enterpriseName}</td>
                <td className="border px-4 py-2">{subcontractor.address}</td>
                <td className="border px-4 py-2">{subcontractor.lot}</td>
                <td className="border px-4 py-2">{subcontractor.contact}</td>
                <td className="border px-4 py-2">{subcontractor.email}</td>
                <td className="border px-4 py-2">
                  <div className="flex">
                    <button
                      className="bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded-md shadow-md mr-2"
                      onClick={() => openEmailHistory(subcontractor)}
                    >
                      Email History
                    </button>
                    <button
                      className="bg-green-500 hover:bg-green-600 text-white px-4 py-2 rounded-md shadow-md"
                      onClick={handleResendEmail}
                    >
                      Resend Email
                    </button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>

      {/* Modal for Email History */}
      {showEmailHistory && (
        <div className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50">
          <div className="bg-white p-6 rounded-lg shadow-lg">
            <div className="flex justify-between items-center mb-4">
              <h3 className="text-lg font-semibold">Email History for {selectedSubcontractor.enterpriseName}</h3>
              <button
                className="text-gray-500 hover:text-gray-700"
                onClick={closeEmailHistory}
              >
                Close
              </button>
            </div>
            <div className="overflow-y-auto max-h-96">
              <ul className="divide-y divide-gray-200">
                {emailHistoryData.map(history => (
                  <li key={history.id} className="py-2">
                    <p className="font-semibold">{history.event}</p>
                    <p>{history.details}</p>
                    <p className="text-gray-500">{new Date(history.date).toLocaleString()}</p>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      )}

      {/* Upload File Popup */}
      {showUploadPopup && (
        <div className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50">
          <div className="bg-white p-6 rounded-lg shadow-lg">
            <h3 className="text-lg font-semibold mb-4">Upload Files</h3>
            {/* Your file upload component or form goes here */}
            <div className="flex justify-end">
              <button
                className="bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded-md shadow-md"
                onClick={() => setShowUploadPopup(false)}
              >
                Close
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default SubcontractorDetails;
