// SubcontractorView.js

import React from 'react';
import { Link } from 'react-router-dom'; // Import Link from react-router-dom
import subcontractorsData from '../../data/lots_constractor.json'; // Assuming subcontractors data is fetched from JSON

const SubcontractorView = () => {
  return (
    <div className="p-6">
      <h2 className="text-2xl font-bold mb-4">Subcontractor List</h2>
      <div className="overflow-x-auto">
        <table className="min-w-full bg-white border border-gray-200">
          <thead className="bg-gray-100">
            <tr>
              <th className="border-b border-gray-200 px-6 py-3 text-left text-sm font-semibold text-gray-700 uppercase tracking-wider">
                Name
              </th>
              <th className="border-b border-gray-200 px-6 py-3 text-left text-sm font-semibold text-gray-700 uppercase tracking-wider">
                Email
              </th>
              <th className="border-b border-gray-200 px-6 py-3 text-left text-sm font-semibold text-gray-700 uppercase tracking-wider">
                Number of Companies
              </th>
              <th className="border-b border-gray-200 px-6 py-3 text-left text-sm font-semibold text-gray-700 uppercase tracking-wider">
                Status
              </th>
              <th className="border-b border-gray-200 px-6 py-3 text-left text-sm font-semibold text-gray-700 uppercase tracking-wider">
                Document Uploaded
              </th>
            </tr>
          </thead>
          <tbody>
            {subcontractorsData.map((subcontractor) => (
              <tr key={subcontractor.id} className="hover:bg-gray-50">
                <td className="border-b border-gray-200 px-6 py-4 whitespace-nowrap">
                  <Link to={`/subcontractors/${subcontractor.id}`} className="text-blue-500 hover:underline">
                    {subcontractor.name}
                  </Link>
                </td>
                <td className="border-b border-gray-200 px-6 py-4 whitespace-nowrap">
                  {subcontractor.email}
                </td>
                <td className="border-b border-gray-200 px-6 py-4 whitespace-nowrap">
                  {subcontractor.numberOfCompanies}
                </td>
                <td className="border-b border-gray-200 px-6 py-4 whitespace-nowrap">
                  {subcontractor.status}
                </td>
                <td className="border-b border-gray-200 px-6 py-4 whitespace-nowrap">
                  {subcontractor.documentUploaded ? 'Yes' : 'No'}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default SubcontractorView;
