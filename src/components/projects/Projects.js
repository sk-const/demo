import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import projectsData from '../../data/projects.json';

const Projects = () => {
  const [showModal, setShowModal] = useState(false);
  const [newProject, setNewProject] = useState({ id: '', name: '', description: '' });

  const columns = projectsData.length > 0 ? Object.keys(projectsData[0]) : [];

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setNewProject(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const handleCreateProject = () => {
    // Simulated logic to add newProject to projectsData
    projectsData.push(newProject);
    setShowModal(false);
    setNewProject({ id: '', name: '', description: '' });
  };

  return (
    <div className="p-6">
      <div className="flex justify-between items-center mb-4">
        <h2 className="text-2xl font-bold">Projects</h2>
        <button
          className="bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded-md shadow-md"
          onClick={() => setShowModal(true)}
        >
          Create Project
        </button>
      </div>
      <div className="overflow-x-auto">
        <table className="min-w-full bg-white border border-gray-200">
          <thead>
            <tr className="bg-gray-100">
              {columns.map(column => (
                <th key={column} className="border-b border-gray-200 px-6 py-3 text-left text-sm font-semibold text-gray-700 uppercase tracking-wider">
                  {column}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {projectsData.map(project => (
              <tr key={project.id} className="hover:bg-gray-50">
                {columns.map(column => (
                  <td key={column} className="border-b border-gray-200 px-6 py-4 whitespace-nowrap">
                    {column === 'name' ? (
                      <Link to={`/projects/${project.id}`} className="text-blue-500 hover:underline">
                        {project[column]}
                      </Link>
                    ) : (
                      project[column]
                    )}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>

      {/* Modal for creating a new project */}
      {showModal && (
        <div className="fixed inset-0 z-50 flex items-center justify-center overflow-auto bg-gray-500 bg-opacity-50">
          <div className="bg-white w-1/2 p-6 rounded-lg shadow-lg">
            <h3 className="text-lg font-semibold mb-4">Create New Project</h3>
            <form>
              <div className="mb-4">
                <label htmlFor="name" className="block text-sm font-medium text-gray-700">Name</label>
                <input type="text" id="name" name="name" value={newProject.name} onChange={handleInputChange} className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" />
              </div>
              <div className="mb-4">
                <label htmlFor="description" className="block text-sm font-medium text-gray-700">Description</label>
                <textarea id="description" name="description" value={newProject.description} onChange={handleInputChange} className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" />
              </div>
              <div className="flex justify-end">
                <button type="button" onClick={() => setShowModal(false)} className="mr-4 bg-gray-300 hover:bg-gray-400 text-gray-800 py-2 px-4 rounded-md">Cancel</button>
                <button type="button" onClick={handleCreateProject} className="bg-blue-500 hover:bg-blue-600 text-white py-2 px-4 rounded-md">Create</button>
              </div>
            </form>
          </div>
        </div>
      )}
    </div>
  );
};

export default Projects;
