import React, { useState } from 'react';
import staffData from '../../data/staff_chantier.json';
import availableStaff from '../../data/staffs.json';
import { useParams } from 'react-router-dom';

const Staff = () => {
  const { chantierId } = useParams();

  // Find the staff list based on chantierId
  const chantierStaff = staffData.find(chantier => chantier.chantierId === parseInt(chantierId));

  const [staffList, setStaffList] = useState(chantierStaff ? chantierStaff.staff : []);
  const [selectedStaff, setSelectedStaff] = useState('');
  const [isModalOpen, setIsModalOpen] = useState(false);

  if (!chantierStaff) {
    return <div>No staff information available for this chantier.</div>;
  }

  // Handle assigning staff to the list
  const handleAssignStaff = () => {
    const staffToAssign = availableStaff.find(staff => staff.name === selectedStaff);
    if (staffToAssign && !staffList.some(staff => staff.name === staffToAssign.name)) {
      // Create a new object without the 'id' field
      const { id, ...newStaff } = staffToAssign;
      setStaffList([...staffList, newStaff]);
    }
    setSelectedStaff('');
    setIsModalOpen(false);
  };

  // Filter available staff to show in dropdown
  const filteredStaff = availableStaff.filter(staff =>
    staff.name.toLowerCase().includes(selectedStaff.toLowerCase())
  );

  return (
    <div className="p-6 relative">
      <h3 className="text-lg font-bold mb-4">Staff</h3>
      <button
        onClick={() => setIsModalOpen(true)}
        className="absolute top-0 right-0 bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded-md shadow-md mt-4 mr-4"
      >
        Assign Staff
      </button>

      {isModalOpen && (
        <div className="fixed inset-0 bg-gray-600 bg-opacity-50 flex items-center justify-center z-50">
          <div className="bg-white rounded-lg shadow-lg w-1/3 p-6">
            <h4 className="text-lg font-bold mb-4">Assign Staff</h4>
            
            <div className="relative">
              <select
                value={selectedStaff}
                onChange={(e) => setSelectedStaff(e.target.value)}
                className="w-full px-4 py-2 border rounded-md appearance-none"
              >
                <option value="">Select Staff</option>
                {filteredStaff.map((staff) => (
                  <option key={staff.name} value={staff.name}>
                    {staff.name} - {staff.role}
                  </option>
                ))}
              </select>
              <div className="absolute inset-y-0 right-0 flex items-center px-2 pointer-events-none">
                <svg className="w-4 h-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                  <path
                    fillRule="evenodd"
                    d="M10 3a1 1 0 00-.832.445L3.516 10H6a1 1 0 110 2H3.516l5.652 6.555A1 1 0 0010 17V3z"
                    clipRule="evenodd"
                  />
                </svg>
              </div>
            </div>
            
            <div className="flex justify-end mt-4 space-x-4">
              <button
                onClick={() => setIsModalOpen(false)}
                className="bg-gray-300 hover:bg-gray-400 text-black px-4 py-2 rounded-md shadow-md"
              >
                Cancel
              </button>
              <button
                onClick={handleAssignStaff}
                className="bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded-md shadow-md"
              >
                Assign
              </button>
            </div>
          </div>
        </div>
      )}
      
      <div className="overflow-x-auto mt-8">
        <table className="min-w-full bg-white border border-gray-200">
          <thead className="bg-gray-100">
            <tr>
              {Object.keys(staffList[0] || {}).map((column) => (
                <th
                  key={column}
                  className="border-b border-gray-200 px-6 py-3 text-left text-sm font-semibold text-gray-700 uppercase tracking-wider"
                >
                  {column}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {staffList.map((member, index) => (
              <tr key={index} className="hover:bg-gray-50">
                {Object.keys(member).map((key) => (
                  <td
                    key={key}
                    className="border-b border-gray-200 px-6 py-4 whitespace-nowrap"
                  >
                    {member[key]}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Staff;
