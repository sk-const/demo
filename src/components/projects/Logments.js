import React, { useState } from 'react';
import chantierLogmentsData from '../../data/chantier_logment.json';

const Logments = ({ chantierId }) => {
  const [isAddFloorModalOpen, setIsAddFloorModalOpen] = useState(false);
  const [floors, setFloors] = useState(chantierLogmentsData.find(item => item.chantierId === chantierId)?.floors || []);

  const [editFloorIndex, setEditFloorIndex] = useState(null);
  const [editNumLogments, setEditNumLogments] = useState(0);

  const [addMultiFloorModalOpen, setAddMultiFloorModalOpen] = useState(false);
  const [numFloorsToAdd, setNumFloorsToAdd] = useState(1);
  const [numLogmentsPerFloor, setNumLogmentsPerFloor] = useState(1);

  const handleAddFloor = () => {
    setIsAddFloorModalOpen(true);
  };

  const handleAddMultiFloor = () => {
    setAddMultiFloorModalOpen(true);
  };

  const handleAddMultiFloorSubmit = () => {
    const newRs = Array.from({ length: numFloorsToAdd }, (_, floorIndex) => {
      const floorNumber = floors.length + 1 + floorIndex;
      return {
        floorNumber: floorNumber,
        floorName: `R+${floorNumber}`, // Updated format for floor name
        logments: Array.from({ length: numLogmentsPerFloor }, (_, logmentIndex) => ({
          logmentId: `${floorNumber}${logmentIndex + 1}`,
          logmentName: `Logment ${logmentIndex + 1}`
        }))
      };
    });

    setFloors([...floors, ...newRs]);

    // Close modal and reset state
    setAddMultiFloorModalOpen(false);
    setNumFloorsToAdd(1);
    setNumLogmentsPerFloor(1);
  };

  const handleAddFloorSubmit = () => {
    const newFloorNumber = floors.length + 1;
    const newFloor = {
      floorNumber: newFloorNumber,
      floorName: `R+${newFloorNumber}`, // Updated format for floor name
      logments: Array.from({ length: editNumLogments }, (_, index) => ({
        logmentId: `${newFloorNumber}${index + 1}`, // Generate logmentId based on floor number and logment index (for demo)
        logmentName: `Logment ${index + 1}` // Generate logment name (for demo)
      }))
    };

    setFloors([...floors, newFloor]);

    // Close modal and reset state
    setIsAddFloorModalOpen(false);
    setEditNumLogments(0);
  };

  const handleEditFloor = (index) => {
    setEditFloorIndex(index);
    setEditNumLogments(floors[index].logments.length);
  };

  const handleUpdateFloor = () => {
    const updatedFloors = [...floors];
    updatedFloors[editFloorIndex] = {
      ...updatedFloors[editFloorIndex],
      logments: Array.from({ length: editNumLogments }, (_, index) => ({
        logmentId: `${editFloorIndex + 1}${index + 1}`, // Generate logmentId based on floor index and logment index (for demo)
        logmentName: `Logment ${index + 1}` // Generate logment name (for demo)
      }))
    };

    setFloors(updatedFloors);

    // Reset edit state
    setEditFloorIndex(null);
    setEditNumLogments(0);
  };

  return (
    <div className="p-6 relative">
      <h3 className="text-lg font-bold mb-4">Chantier Logments - Chantier ID: {chantierId}</h3>
      <button
        onClick={handleAddFloor}
        className="bg-green-500 hover:bg-green-600 text-white px-3 py-1 rounded-md shadow-md absolute top-0 right-0 mt-3 mr-3"
      >
        Add R
      </button>
      <button
        onClick={handleAddMultiFloor}
        className="bg-blue-500 hover:bg-blue-600 text-white px-3 py-1 rounded-md shadow-md absolute top-0 right-20 mt-3 mr-3"
      >
        Add Multiple Rs
      </button>
      {floors.map((floor, floorIndex) => (
        <div key={floorIndex} className="border rounded-md p-4 mb-6">
          {editFloorIndex === floorIndex ? (
            <div className="mb-4">
              <h4 className="text-xl font-semibold mb-2">{floor.floorName}</h4>
              <input
                type="number"
                value={editNumLogments}
                onChange={(e) => setEditNumLogments(parseInt(e.target.value))}
                placeholder="Number of Logments"
                className="w-full px-4 py-2 border rounded-md mb-2"
              />
              <div className="flex justify-end">
                <button
                  onClick={() => setEditFloorIndex(null)}
                  className="bg-gray-300 hover:bg-gray-400 text-black px-4 py-2 rounded-md shadow-md mr-2"
                >
                  Cancel
                </button>
                <button
                  onClick={handleUpdateFloor}
                  className="bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded-md shadow-md"
                >
                  Update R
                </button>
              </div>
            </div>
          ) : (
            <>
              <h4 className="text-xl font-semibold mb-2">{floor.floorName}</h4>
              <ul className="list-disc pl-6">
                {floor.logments.map((logment, logmentIndex) => (
                  <li key={logment.logmentId} className="mb-2">{`${logment.logmentName}`}</li>
                ))}
              </ul>
              <div className="flex justify-end">
                <button
                  onClick={() => handleEditFloor(floorIndex)}
                  className="bg-yellow-500 hover:bg-yellow-600 text-white px-3 py-1 rounded-md shadow-md mt-3 mr-2"
                >
                  Edit Logments
                </button>
              </div>
            </>
          )}
        </div>
      ))}
      
      {/* Add R Modal */}
      {isAddFloorModalOpen && (
        <div className="fixed inset-0 bg-gray-600 bg-opacity-50 flex items-center justify-center z-50">
          <div className="bg-white rounded-lg shadow-lg w-1/3 p-6">
            <h4 className="text-lg font-bold mb-4">Add R</h4>
            <div className="mb-4">
              <input
                type="number"
                value={editNumLogments}
                onChange={(e) => setEditNumLogments(parseInt(e.target.value))}
                placeholder="Number of Logments"
                className="w-full px-4 py-2 border rounded-md mb-2"
              />
            </div>
            <div className="flex justify-end">
              <button
                onClick={() => setIsAddFloorModalOpen(false)}
                className="bg-gray-300 hover:bg-gray-400 text-black px-4 py-2 rounded-md shadow-md mr-2"
              >
                Cancel
              </button>
              <button
                onClick={handleAddFloorSubmit}
                className="bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded-md shadow-md"
              >
                Add R
              </button>
            </div>
          </div>
        </div>
      )}

      {/* Add Multiple Rs Modal */}
      {addMultiFloorModalOpen && (
        <div className="fixed inset-0 bg-gray-600 bg-opacity-50 flex items-center justify-center z-50">
          <div className="bg-white rounded-lg shadow-lg w-1/3 p-6">
            <h4 className="text-lg font-bold mb-4">Add Multiple Rs with Logments</h4>
            <div className="mb-4">
              <label className="block mb-2">Number of Rs:</label>
              <input
                type="number"
                value={numFloorsToAdd}
                onChange={(e) => setNumFloorsToAdd(parseInt(e.target.value))}
                className="w-full px-4 py-2 border rounded-md mb-2"
              />
              <label className="block mb-2">Number of Logments per R:</label>
              <input
                type="number"
                value={numLogmentsPerFloor}
                onChange={(e) => setNumLogmentsPerFloor(parseInt(e.target.value))}
                className="w-full px-4 py-2 border rounded-md mb-2"
              />
            </div>
            <div className="flex justify-end">
              <button
                onClick={() => setAddMultiFloorModalOpen(false)}
                className="bg-gray-300 hover:bg-gray-400 text-black px-4 py-2 rounded-md shadow-md mr-2"
              >
                Cancel
              </button>
              <button
                onClick={handleAddMultiFloorSubmit}
                className="bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded-md shadow-md"
              >
                Add Rs with Logments
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Logments;
