import React from 'react';
import projectsData from '../../data/projects.json';
import chantiersData from '../../data/chantiers.json';
import { useParams, Link } from 'react-router-dom';

const ProjectDetails = () => {
  // Get project ID from URL parameters
  const { projectId } = useParams();

  // Find the project object based on projectId
  const project = projectsData.find(project => project.id === parseInt(projectId));

  // Filter chantiers based on projectId
  const chantiers = chantiersData.filter(chantier => chantier.projectId === parseInt(projectId));

  if (!project) {
    return <div>Loading...</div>; // or handle if project is not found
  }

  // Determine dynamic columns based on the first chantier object
  const columns = chantiers.length > 0 ? Object.keys(chantiers[0]) : [];

  return (
    <div className="p-6">
      <div className="flex items-center mb-4">
        <p className="text-sm text-gray-500 ml-1">
          <Link to="/projects" className="text-sm text-gray-500 hover:underline">
            project/{project.name}
          </Link>
        </p>
      </div>
      <div className="bg-white border border-gray-200 rounded-lg p-4">
      <h1>{project.name}</h1>
        <p>{project.description}</p>
        
        <h3 className="text-lg font-bold mt-4">Chantiers</h3>
        <div className="overflow-x-auto">
          <table className="min-w-full bg-white border border-gray-200">
            <thead>
              <tr className="bg-gray-100">
                {columns.map(column => (
                  <th key={column} className="border-b border-gray-200 px-6 py-3 text-left text-sm font-semibold text-gray-700 uppercase tracking-wider">
                    {column}
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {chantiers.map(chantier => (
                <tr key={chantier.id} className="hover:bg-gray-50">
                  {columns.map(column => (
                    <td key={column} className="border-b border-gray-200 px-6 py-4 whitespace-nowrap">
                      <Link to={`/projects/${project.id}/chantiers/${chantier.id}`} className="text-blue-500 hover:underline">
                        {chantier[column]}
                      </Link>
                    </td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        
      </div>
    </div>
  );
};

export default ProjectDetails;
