// src/components/ChantierDetails.js

import React, { useState } from 'react';
import chantiersData from '../../data/chantiers.json';
import { useParams, Link } from 'react-router-dom';
import GeneralInfo from './GeneralInfo';
import InitialPlan from './InitialPlan';
import Staff from './Staff';
import Logments from './Logments';
import SubcontractorView from './SubcontractorView'; // Import SubcontractorView

const ChantierDetails = () => {
  const { projectId, chantierId } = useParams();
  const chantier = chantiersData.find(chantier => chantier.id === parseInt(chantierId) && chantier.projectId === parseInt(projectId));
  const [activeTab, setActiveTab] = useState('general');

  if (!chantier) {
    return <div>Loading...</div>;
  }

  return (
    <div className="p-6">
      <div className="flex items-center mb-4">
        <p className="text-sm text-gray-500 ml-1">
          <Link to={`/projects/${projectId}`} className="text-sm text-gray-500 hover:underline">
            project/{projectId}
          </Link>
        </p>
      </div>
      
      {/* Tab Buttons Section (Centered) */}
      <div className="flex justify-center mb-4">
        <button
          className={`py-2 px-4 ${activeTab === 'general' ? 'border-b-2 border-blue-500 font-semibold' : ''}`}
          onClick={() => setActiveTab('general')}
        >
          General Information
        </button>
        <button
          className={`py-2 px-4 ${activeTab === 'plan' ? 'border-b-2 border-blue-500 font-semibold' : ''}`}
          onClick={() => setActiveTab('plan')}
        >
          Initial Plan
        </button>
        <button
          className={`py-2 px-4 ${activeTab === 'staff' ? 'border-b-2 border-blue-500 font-semibold' : ''}`}
          onClick={() => setActiveTab('staff')}
        >
          Staff
        </button>
        <button
          className={`py-2 px-4 ${activeTab === 'logments' ? 'border-b-2 border-blue-500 font-semibold' : ''}`}
          onClick={() => setActiveTab('logments')}
        >
          Logments
        </button>
        <button
          className={`py-2 px-4 ${activeTab === 'subcontractors' ? 'border-b-2 border-blue-500 font-semibold' : ''}`}
          onClick={() => setActiveTab('subcontractors')}
        >
          Subcontractors
        </button>
      </div>

      <div className="bg-white border border-gray-200 rounded-lg p-4">
        <div className="mt-4">
          {activeTab === 'general' && <GeneralInfo description={chantier.description} responsables={chantier.responsables} name={chantier.name} />}
          {activeTab === 'plan' && <InitialPlan />}
          {activeTab === 'staff' && <Staff />}
          {activeTab === 'logments' && <Logments chantierId={chantier.id} />}
          {activeTab === 'subcontractors' && <SubcontractorView />}
        </div>
      </div>
    </div>
  );
};

export default ChantierDetails;
