import React from 'react';

const GeneralInfo = ({ description, responsables,name }) => {
  return (
    <div>
           <h2 className="text-lg font-bold">{name}</h2>
      <p>{description}</p>
      <div className="mt-4">
        <h3 className="text-lg font-bold">Responsables</h3>
        <ul className="list-disc list-inside">
          {responsables.map((responsable, index) => (
            <li key={index}>{responsable}</li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default GeneralInfo;
