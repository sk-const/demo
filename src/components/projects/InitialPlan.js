import React, { useState } from 'react';
import initPlanData from '../../data/init_plan.json';
import lots from '../../data/lots.json'; // Assuming you have lots.json in the correct location

const InitialPlan = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [eventName, setEventName] = useState('');
  const [lot, setLot] = useState('');
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [color, setColor] = useState('#0099FF'); // Default color

  const [error, setError] = useState(null);

  const handleAddEvent = () => {
    // Validate dates
    if (!eventName || !startDate || !endDate) {
      setError('Please fill in all fields.');
      return;
    }

    // Check if start date is before or the same as end date
    const start = new Date(startDate);
    const end = new Date(endDate);
    if (start.getTime() > end.getTime()) {
      setError('Start Date cannot be after End Date.');
      return;
    }

    // Logic to add event to initPlanData
    const newEvent = {
      name: eventName,
      lot: lot || null,
      startDate: start.toISOString(),
      endDate: end.toISOString(),
      color: color
    };
    initPlanData.push(newEvent);

    // Clear form fields, reset error, and close modal
    setEventName('');
    setLot('');
    setStartDate('');
    setEndDate('');
    setColor('#0099FF');
    setError(null);
    setIsModalOpen(false);
  };

  // Extract columns dynamically from the first item in initPlanData
  const columns = initPlanData.length > 0 ? Object.keys(initPlanData[0]) : [];

  return (
    <div className="p-6 relative">
      <h3 className="text-lg font-bold mb-4">Initial Plan</h3>
      <div className="absolute top-0 right-0 mt-4 mr-4">
        <button
          onClick={() => setIsModalOpen(true)}
          className="bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded-md shadow-md"
        >
          Add Event
        </button>
      </div>
      <div className="overflow-x-auto mt-10">
        <table className="min-w-full bg-white border border-gray-200">
          <thead>
            <tr className="bg-gray-100">
              {columns.map(column => (
                <th
                  key={column}
                  className="border-b border-gray-200 px-6 py-3 text-left text-sm font-semibold text-gray-700 uppercase tracking-wider"
                >
                  {column}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {initPlanData.map((item, index) => (
              <tr key={index} className="hover:bg-gray-50" style={{ backgroundColor: item.color }}>
                {columns.map((column, columnIndex) => (
                  <td
                    key={columnIndex}
                    className="border-b border-gray-200 px-6 py-4 whitespace-nowrap"
                  >
                    {item[column]}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>

      {isModalOpen && (
        <div className="fixed inset-0 bg-gray-600 bg-opacity-50 flex items-center justify-center z-50">
          <div className="bg-white rounded-lg shadow-lg w-1/3 p-6">
            <h4 className="text-lg font-bold mb-4">Add Event</h4>
            {error && <div className="text-red-500 mb-2">{error}</div>}
            <div className="mb-4">
              <input
                type="text"
                value={eventName}
                onChange={(e) => setEventName(e.target.value)}
                placeholder="Event Name"
                className="w-full px-4 py-2 border rounded-md mb-2"
              />
              <select
                value={lot}
                onChange={(e) => setLot(e.target.value)}
                className="w-full px-4 py-2 border rounded-md mb-2"
              >
                <option value="">Select Lot (optional)</option>
                {lots.map(lot => (
                  <option key={lot.id} value={lot.name}>{lot.name}</option>
                ))}
              </select>
              <input
                type="date"
                value={startDate}
                onChange={(e) => setStartDate(e.target.value)}
                placeholder="Start Date"
                className="w-full px-4 py-2 border rounded-md mb-2"
              />
              <input
                type="date"
                value={endDate}
                onChange={(e) => setEndDate(e.target.value)}
                placeholder="End Date"
                className="w-full px-4 py-2 border rounded-md mb-2"
              />
              <input
                type="color"
                value={color}
                onChange={(e) => setColor(e.target.value)}
                className="w-full px-4 py-2 border rounded-md mb-2"
              />
            </div>
            <div className="flex justify-end">
              <button
                onClick={() => setIsModalOpen(false)}
                className="bg-gray-300 hover:bg-gray-400 text-black px-4 py-2 rounded-md shadow-md mr-2"
              >
                Cancel
              </button>
              <button
                onClick={handleAddEvent}
                className="bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded-md shadow-md"
              >
                Add
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default InitialPlan;
