// src/components/SubcontractorCompany.js
import React, { useState } from 'react';
import lotsData from '../../data/lots.json';
import subcontractorCompaniesData from '../../data/subcontractorCompanies.json';

const SubcontractorCompany = () => {
  const [showModal, setShowModal] = useState(false);
  const [newCompany, setNewCompany] = useState({
    id: '',
    enterpriseName: '',
    address: '',
    lot: '',
    contact: '',
    email: ''
  });
  const [filterCriteria, setFilterCriteria] = useState({
    enterpriseName: '',
    lot: ''
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setNewCompany(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const handleCreateCompany = () => {
    // Add newCompany to subcontractorCompaniesData (simulated here, replace with actual logic)
    subcontractorCompaniesData.push(newCompany);
    // Close the modal after creating the company
    setShowModal(false);
    // Clear input fields after creating the company
    setNewCompany({
      id: '',
      enterpriseName: '',
      address: '',
      lot: '',
      contact: '',
      email: ''
    });
  };

  const filteredCompanies = subcontractorCompaniesData.filter(company => {
    return (
      company.enterpriseName.toLowerCase().includes(filterCriteria.enterpriseName.toLowerCase()) &&
      (filterCriteria.lot === '' || company.lot === filterCriteria.lot)
    );
  });

  const handleFilterChange = (e) => {
    const { name, value } = e.target;
    setFilterCriteria(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  return (
    <div className="p-6">
      <div className="flex justify-between items-center mb-4">
        <h2 className="text-2xl font-bold">Subcontractor Companies</h2>
        <button
          className="bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded-md shadow-md"
          onClick={() => setShowModal(true)}
        >
          Create Company
        </button>
      </div>

      {/* Filter section */}
      <div className="mb-4 flex space-x-4">
        <input
          type="text"
          placeholder="Search by Enterprise Name"
          name="enterpriseName"
          value={filterCriteria.enterpriseName}
          onChange={handleFilterChange}
          className="border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm px-4 py-2"
        />
        <select
          name="lot"
          value={filterCriteria.lot}
          onChange={handleFilterChange}
          className="border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm px-4 py-2"
        >
          <option value="">Select Lot</option>
          {lotsData.map(lot => (
            <option key={lot.id} value={lot.name}>{lot.name}</option>
          ))}
        </select>
      </div>

      {/* Company list */}
      <div className="overflow-x-auto">
        <table className="min-w-full bg-white border border-gray-200">
          <thead>
            <tr className="bg-gray-100">
              <th className="border-b border-gray-200 px-6 py-3 text-left text-sm font-semibold text-gray-700 uppercase tracking-wider">Enterprise Name</th>
              <th className="border-b border-gray-200 px-6 py-3 text-left text-sm font-semibold text-gray-700 uppercase tracking-wider">Address</th>
              <th className="border-b border-gray-200 px-6 py-3 text-left text-sm font-semibold text-gray-700 uppercase tracking-wider">Lot</th>
              <th className="border-b border-gray-200 px-6 py-3 text-left text-sm font-semibold text-gray-700 uppercase tracking-wider">Contact</th>
              <th className="border-b border-gray-200 px-6 py-3 text-left text-sm font-semibold text-gray-700 uppercase tracking-wider">Email</th>
            </tr>
          </thead>
          <tbody>
            {filteredCompanies.map(company => (
              <tr key={company.id} className="hover:bg-gray-50">
                <td className="border-b border-gray-200 px-6 py-4">{company.enterpriseName}</td>
                <td className="border-b border-gray-200 px-6 py-4">{company.address}</td>
                <td className="border-b border-gray-200 px-6 py-4">{company.lot}</td>
                <td className="border-b border-gray-200 px-6 py-4">{company.contact}</td>
                <td className="border-b border-gray-200 px-6 py-4">{company.email}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>

      {/* Modal for creating a new subcontractor company */}
      {showModal && (
        <div className="fixed inset-0 z-50 flex items-center justify-center overflow-auto bg-gray-500 bg-opacity-50">
          <div className="bg-white w-1/2 p-6 rounded-lg shadow-lg">
            <h3 className="text-lg font-semibold mb-4">Create New Subcontractor Company</h3>
            <form>
              <div className="mb-4">
                <label htmlFor="enterpriseName" className="block text-sm font-medium text-gray-700">Enterprise Name</label>
                <input type="text" id="enterpriseName" name="enterpriseName" value={newCompany.enterpriseName} onChange={handleInputChange} className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" />
              </div>
              <div className="mb-4">
                <label htmlFor="address" className="block text-sm font-medium text-gray-700">Address</label>
                <input type="text" id="address" name="address" value={newCompany.address} onChange={handleInputChange} className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" />
              </div>
              <div className="mb-4">
                <label htmlFor="lot" className="block text-sm font-medium text-gray-700">Lot</label>
                <select id="lot" name="lot" value={newCompany.lot} onChange={handleInputChange} className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                  <option value="">Select Lot</option>
                  {lotsData.map(lot => (
                    <option key={lot.id} value={lot.name}>{lot.name}</option>
                  ))}
                </select>
              </div>
              <div className="mb-4">
                <label htmlFor="contact" className="block text-sm font-medium text-gray-700">Contact</label>
                <input type="text" id="contact" name="contact" value={newCompany.contact} onChange={handleInputChange} className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" />
              </div>
              <div className="mb-4">
                <label htmlFor="email" className="block text-sm font-medium text-gray-700">Email</label>
                <input type="email" id="email" name="email" value={newCompany.email} onChange={handleInputChange} className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" />
              </div>
              <div className="flex justify-end">
                <button type="button" onClick={() => setShowModal(false)} className="mr-4 bg-gray-300 hover:bg-gray-400 text-gray-800 py-2 px-4 rounded-md">Cancel</button>
                <button type="button" onClick={handleCreateCompany} className="bg-blue-500 hover:bg-blue-600 text-white py-2 px-4 rounded-md">Create</button>
              </div>
            </form>
          </div>
        </div>
      )}
    </div>
  );
};

export default SubcontractorCompany;
