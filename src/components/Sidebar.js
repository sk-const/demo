// src/components/Sidebar.js
import React from 'react';
import { Link } from 'react-router-dom';
import sidebarItems from '../data/sidebarItems.json';

const Sidebar = () => {
  return (
    <div className="h-screen w-64 bg-white text-gray-800 flex flex-col shadow-lg">
      <div className="flex items-center justify-center h-16 bg-gray-100 border-b">
        <h1 className="text-xl font-bold text-gray-800">Dashboard</h1>
      </div>
      <nav className="flex-1 px-4 py-6">
        <ul>
          {sidebarItems.map(item => (
            <li key={item.id} className="mb-4">
              <Link 
                to={`/${item.id}`}
                className="flex items-center px-4 py-2 text-gray-700 hover:bg-gray-200 rounded-md transition-colors duration-200"
              >
                {item.icon && (
                  <span className="mr-4 text-gray-500">
                    <i className={`fas fa-${item.icon}`}></i>
                  </span>
                )}
                <span className="text-sm font-medium">{item.label}</span>
              </Link>
            </li>
          ))}
        </ul>
      </nav>
    </div>
  );
};

export default Sidebar;
