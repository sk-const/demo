import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import staffData from '../../data/staffs.json';

const UserDetails = () => {
  const { userId } = useParams();
  const [staff, setStaff] = useState({ name: '', email: '', phone: '', roles: [], permissions: [], permissionGroups: [] });
  const [selectedRole, setSelectedRole] = useState('');
  const [selectedPermission, setSelectedPermission] = useState('');
  const [newPermissionGroup, setNewPermissionGroup] = useState('');

  useEffect(() => {
    const fetchStaffData = async () => {
      try {
        const user = staffData.find(user => user.id === parseInt(userId));
        if (user) {
          setStaff({
            ...user,
            permissions: user.permissions || [], // Initialize permissions as empty array if undefined
            permissionGroups: user.permissionGroups || [], // Initialize permissionGroups as empty array if undefined
          });
          setSelectedRole(user.roles.length > 0 ? user.roles[0] : '');
        }
      } catch (error) {
        console.error('Error fetching user data:', error);
      }
    };

    fetchStaffData();
  }, [userId]);

  const roles = ['Admin', 'Manager', 'Developer', 'Support'];
  const permissions = ['Create', 'Read', 'Update', 'Delete'];

  const handleRoleChange = (e) => {
    setSelectedRole(e.target.value);
  };

  const handlePermissionChange = (e) => {
    setSelectedPermission(e.target.value);
  };

  const handleAddPermissionGroup = () => {
    // Implement logic to add new permission group
    console.log('New permission group added:', newPermissionGroup);
    setNewPermissionGroup('');
  };

  if (!staff.name) {
    return (
      <div className="p-6">
        <h2 className="text-2xl font-bold mb-4">User Details</h2>
        <div className="border border-gray-200 rounded-lg p-4">
          <p className="text-gray-500">User information not found.</p>
        </div>
      </div>
    );
  }

  return (
    <div className="p-6">
      <h2 className="text-2xl font-bold mb-4">User Details - {staff.name}</h2>
      <div className="border border-gray-200 rounded-lg p-4 mb-4">
        <div className="mb-4">
          <p className="font-semibold">Name:</p>
          <p>{staff.name}</p>
        </div>
        <div className="mb-4">
          <p className="font-semibold">Email:</p>
          <p>{staff.email}</p>
        </div>
        <div className="mb-4">
          <p className="font-semibold">Phone:</p>
          <p>{staff.phone}</p>
        </div>
      </div>
      <div className="border border-gray-200 rounded-lg p-4 mb-4">
        <h3 className="text-lg font-semibold mb-4">Assign Role</h3>
        <select
          value={selectedRole}
          onChange={handleRoleChange}
          className="px-4 py-2 border border-gray-300 rounded-md w-full"
        >
          {roles.map(role => (
            <option key={role} value={role}>{role}</option>
          ))}
        </select>
      </div>
      <div className="border border-gray-200 rounded-lg p-4 mb-4">
        <h3 className="text-lg font-semibold mb-4">Assign Permissions</h3>
        <div className="flex items-center mb-2">
          <select
            value={selectedPermission}
            onChange={handlePermissionChange}
            className="px-4 py-2 border border-gray-300 rounded-md mr-2"
          >
            <option value="">Select Permission</option>
            {permissions.map(permission => (
              <option key={permission} value={permission}>{permission}</option>
            ))}
          </select>
          <button
            className="bg-blue-500 hover:bg-blue-600 text-white py-2 px-4 rounded-md"
            onClick={() => console.log('Add permission clicked')} // Implement add permission logic
          >
            Add Permission
          </button>
        </div>
        <ul className="list-disc list-inside">
          {staff.permissions.map((permission, index) => (
            <li key={index}>{permission}</li>
          ))}
        </ul>
      </div>
      <div className="border border-gray-200 rounded-lg p-4">
        <h3 className="text-lg font-semibold mb-4">Add Permission Group</h3>
        <div className="flex items-center">
          <input
            type="text"
            placeholder="Enter new permission group"
            value={newPermissionGroup}
            onChange={(e) => setNewPermissionGroup(e.target.value)}
            className="px-4 py-2 border border-gray-300 rounded-md mr-2 w-full"
          />
          <button
            className="bg-blue-500 hover:bg-blue-600 text-white py-2 px-4 rounded-md"
            onClick={handleAddPermissionGroup}
          >
            Add Group
          </button>
        </div>
        <ul className="list-disc list-inside">
          {staff.permissionGroups.map((group, index) => (
            <li key={index}>{group}</li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default UserDetails;
