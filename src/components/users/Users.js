import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import staffData from '../../data/staffs.json';

const Users = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [selectedUserId, setSelectedUserId] = useState(null);
  const [creatingStaff, setCreatingStaff] = useState(false);
  const [newStaff, setNewStaff] = useState({
    name: '',
    email: '',
    role: ''
  });
  const [selectedRole, setSelectedRole] = useState('');

  const filteredStaff = staffData.filter(user =>
    user.name.toLowerCase().includes(searchTerm.toLowerCase()) &&
    (selectedRole === '' || user.role.toLowerCase() === selectedRole.toLowerCase())
  );

  const handleSelectUser = (userId) => {
    setSelectedUserId(userId);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setNewStaff(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const handleCreateStaff = () => {
    if (!newStaff.name || !newStaff.email || !newStaff.role) {
      alert('Please fill out all fields.');
      return;
    }

    // Generate a unique ID for the new staff member (replace with actual logic)
    const newStaffMember = {
      id: Math.floor(Math.random() * 1000), // Example: Replace with your ID generation logic
      ...newStaff
    };

    // Update staff data (for demonstration, adding to local array)
    staffData.push(newStaffMember);

    // Clear form fields and close the popup/modal
    setNewStaff({
      name: '',
      email: '',
      role: ''
    });
    setCreatingStaff(false);
  };

  const tableColumns = Object.keys(filteredStaff[0] || {});

  const roles = [...new Set(staffData.map(user => user.role))]; // Get unique roles

  return (
    <div className="p-6 flex flex-col h-full">
      <div className="flex items-center justify-between mb-4">
        <h2 className="text-2xl font-bold">Staff</h2>
        <div className="flex items-center">
          <input
            type="text"
            placeholder="Search by name"
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
            className="px-4 py-2 border border-gray-300 rounded-md mr-4"
          />
          <select
            value={selectedRole}
            onChange={(e) => setSelectedRole(e.target.value)}
            className="px-4 py-2 border border-gray-300 rounded-md mr-4"
          >
            <option value="">All Roles</option>
            {roles.map((role, index) => (
              <option key={index} value={role}>{role}</option>
            ))}
          </select>
          <button
            className="bg-blue-500 hover:bg-blue-600 text-white py-2 px-4 rounded-md mr-4"
            onClick={() => setCreatingStaff(true)} // Toggle popup/modal visibility
          >
            Create Staff
          </button>
        </div>
      </div>

      {creatingStaff && (
        <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center bg-gray-900 bg-opacity-50">
          <div className="bg-white p-6 rounded-lg w-96">
            <h2 className="text-xl font-bold mb-4">Create New Staff</h2>
            <input
              type="text"
              name="name"
              value={newStaff.name}
              onChange={handleInputChange}
              placeholder="Name"
              className="px-4 py-2 border border-gray-300 rounded-md mb-2 w-full"
            />
            <input
              type="email"
              name="email"
              value={newStaff.email}
              onChange={handleInputChange}
              placeholder="Email"
              className="px-4 py-2 border border-gray-300 rounded-md mb-2 w-full"
            />
            <input
              type="text"
              name="role"
              value={newStaff.role}
              onChange={handleInputChange}
              placeholder="Role"
              className="px-4 py-2 border border-gray-300 rounded-md mb-4 w-full"
            />
            <div className="flex justify-end">
              <button
                className="bg-blue-500 hover:bg-blue-600 text-white py-2 px-4 rounded-md mr-2"
                onClick={handleCreateStaff}
              >
                Save
              </button>
              <button
                className="bg-gray-300 hover:bg-gray-400 text-gray-800 py-2 px-4 rounded-md"
                onClick={() => setCreatingStaff(false)} // Close the popup/modal
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      )}

      <div className="flex-1 overflow-y-auto">
        <table className="min-w-full bg-white border border-gray-200 rounded-lg">
          <thead className="bg-gray-100">
            <tr>
              {tableColumns.map(column => (
                <th
                  key={column}
                  className="border-b border-gray-200 px-6 py-3 text-left text-sm font-semibold text-gray-700 uppercase tracking-wider"
                >
                  {column}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {filteredStaff.map(user => (
              <tr
                key={user.id}
                className={`cursor-pointer ${selectedUserId === user.id ? 'bg-blue-100' : ''}`}
                onClick={() => handleSelectUser(user.id)}
              >
                {tableColumns.map(column => (
                  <td
                    key={column}
                    className="border-b border-gray-200 px-6 py-4 whitespace-nowrap"
                  >
                    {column === 'name' ? (
                      <Link to={`/users/${user.id}`} className="text-blue-500 hover:underline">
                        {user[column]}
                      </Link>
                    ) : (
                      user[column]
                    )}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Users;
